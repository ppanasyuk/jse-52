package ru.t1.panasyuk.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.data.DataXMLSaveJaxBRequest;

public final class DataXMLSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file.";

    @NotNull
    private static final String NAME = "data-save-xml-jaxb";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXMLSaveJaxBRequest request = new DataXMLSaveJaxBRequest(getToken());
        getDomainEndpoint().saveDataXMLJaxB(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
