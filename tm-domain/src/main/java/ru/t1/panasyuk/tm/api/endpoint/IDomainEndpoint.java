package ru.t1.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.data.*;
import ru.t1.panasyuk.tm.dto.response.data.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinarySaveRequest request
    );

    @NotNull
    @WebMethod
    DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadFasterXMLRequest request
    );

    @NotNull
    @WebMethod
    DataJsonLoadJaxBResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveFasterXMLRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveJaxBResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataXMLLoadFasterXMLResponse loadDataXMLFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXMLLoadFasterXMLRequest request
    );

    @NotNull
    @WebMethod
    DataXMLLoadJaxBResponse loadDataXMLJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXMLLoadJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataXMLSaveFasterXMLResponse saveDataXMLFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXMLSaveFasterXMLRequest request
    );

    @NotNull
    @WebMethod
    DataXMLSaveJaxBResponse saveDataXMLJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXMLSaveJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataYamlLoadFasterXMLResponse loadDataYamlFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlLoadFasterXMLRequest request
    );

    @NotNull
    @WebMethod
    DataYamlSaveFasterXMLResponse saveDataYamlFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlSaveFasterXMLRequest request
    );

}