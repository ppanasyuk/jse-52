package ru.t1.panasyuk.tm.component;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.IPropertyService;
import ru.t1.panasyuk.tm.api.IReceiverService;
import ru.t1.panasyuk.tm.listener.EntityListener;
import ru.t1.panasyuk.tm.service.PropertyService;
import ru.t1.panasyuk.tm.service.ReceiverService;

public final class Bootstrap {

    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final String activeMqHost = propertyService.getActiveMqHost();
        @NotNull final String activeMqPort = propertyService.getActiveMqPort();
        @NotNull final String Url = "failover://tcp://" + activeMqHost + ":" + activeMqPort;
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(Url);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new EntityListener(propertyService));
    }

}