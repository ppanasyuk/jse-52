package ru.t1.panasyuk.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.ILoggerService;
import ru.t1.panasyuk.tm.api.IPropertyService;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
public final class LoggerService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private MongoClient mongoClient;

    @NotNull
    private MongoDatabase mongoDatabase;

    public LoggerService(@NotNull final IPropertyService propertyService) {
        @NotNull final String mongoClientHost = propertyService.getMongoClientHost();
        @NotNull final int mongoClientPort = Integer.parseInt(propertyService.getMongoClientPort());
        mongoClient = new MongoClient(mongoClientHost, mongoClientPort);
        mongoDatabase = mongoClient.getDatabase("tm_log");
    }

    @Override
    @SneakyThrows
    public void log(@NotNull final String message) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        if (mongoDatabase.getCollection(table) == null) mongoDatabase.createCollection(table);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(table);
        collection.insertOne(new Document(event));
    }

}