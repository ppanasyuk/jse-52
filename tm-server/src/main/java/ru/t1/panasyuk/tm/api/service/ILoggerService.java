package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable String message);

    void initJmsLogger(@NotNull IPropertyService propertyService);

    void command(@Nullable String message);

    void debug(@Nullable String message);

    void error(@Nullable Exception e);

}