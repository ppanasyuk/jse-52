package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.repository.model.ISessionRepository;
import ru.t1.panasyuk.tm.model.Session;

import javax.persistence.EntityManager;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    @Override
    protected Class<Session> getEntityClass() {
        return Session.class;
    }

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}